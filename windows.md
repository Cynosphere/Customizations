# Windows Rice Settings
* Theme: [Paranoid Android](https://www.deviantart.com/niivu/art/Paranoid-Android-Windows-10-Themes-821696974) - lace style
* Icons: [Joyful Desktop (Mechanical)](https://www.deviantart.com/niivu/art/Joyful-Desktop-Windows-10-Theme-858354411) [(mirror)](/icons/7TSP%20mechanical.7z)

## OldNewExplorer settings:
![OldNewExplorer settings](oldnewexplorer.png)