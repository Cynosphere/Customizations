# Discord Customizations
**Sass folders require a Sass compiler.** This is needed for multiwidth guilds.

If you do have a Sass compiler in your client mod or whatever you use for theming, add an entry to point to `theme.scss` along with `discord.css`.

## Credits
* Mary (mstrodl) - Multiwidth Guilds ([found here](https://github.com/Mstrodl/discordtheme/tree/master/largeAesthetic/multiGuildColumns))

## Previews

![](https://totallynotavir.us/i/hjerwti2.png)

![](https://totallynotavir.us/i/wowhgoru.png)

![](https://totallynotavir.us/i/dv39ok6q.png)