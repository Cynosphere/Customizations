# xmc [![Install directly with Stylus](https://img.shields.io/badge/Install%20directly%20with-Stylus-238b8b.svg)](https://gitlab.com/Cynosphere/Customizations/-/raw/master/discord/xmc/xmc.user.css)
A theme originally based off of the [xmc 2.0 Windows Theme](https://www.deviantart.com/niivu/art/xmc-2-0-Windows-10-Theme-829427196), turned into an all purpose, customizable modular theme.

# **IMPORTANT UPDATE APRIL 11, 2022**
XMC is transfering to UserCSS only. Bug your favorite client mod to add UserCSS support :^)
## Why?
Easier for me to manage one file, easier for users to use one file. Also will work in browser now with all the customizations (implying you're using a userstyle manager, and you better be using [Stylus](https://add0n.com/stylus.html)).

<details>
  <summary>Everything onwards is now obsolete</summary>

# Setup
## GooseMod Users
Add `https://cynosphere.gitlab.io/Customizations/discord/xmc/_goosemod/modules.json` to your repos (the cloud icon in the Plugins and Themes menus)

## Base **(REQUIRED)**
```css
@import url("https://cynosphere.gitlab.io/Customizations/discord/xmc/xmc-base.css");
```

## Color Scheme **(REQUIRED)**
**If you do not choose a color scheme you will end up with a gray background, black text and almost everything being transparent.**

Feel free to [request an existing color scheme](https://gitlab.com/Cynosphere/Customizations/-/issues/new) or pull request your own. <sub><sub>(not guaranteed to be accepted, quality control applies)</sub></sub>

### xmc
```css
@import url("https://cynosphere.gitlab.io/Customizations/discord/xmc/colors/xmc.css");
```

![xmc color scheme preview](_previews/schemes/xmc.png)

### [Amora](https://github.com/owozsh/Amora)
```css
@import url("https://cynosphere.gitlab.io/Customizations/discord/xmc/colors/amora.css");
```

![Amora color scheme preview](_previews/schemes/amora.png)

### [Gruvbox](https://github.com/morhetz/gruvbox)
```css
@import url("https://cynosphere.gitlab.io/Customizations/discord/xmc/colors/gruvbox.css");
```

![Gruvbox color scheme preview](_previews/schemes/gruvbox.png)

### Stock
Uses pre-rebrand colors.

```css
@import url("https://cynosphere.gitlab.io/Customizations/discord/xmc/colors/stock.css");
```

![Stock color scheme preview](_previews/schemes/stock.png)

### [Amora Focus](https://github.com/owozsh/Amora)
```css
@import url("https://cynosphere.gitlab.io/Customizations/discord/xmc/colors/amora-focus.css");
```

![Amora Focus color scheme preview](_previews/schemes/amora-focus.png)

### AMOLED
Based off of [0cord](https://gitlab.com/a/0cord), which is dual licensed under CC-BY-NC and MIT.
```css
@import url("https://cynosphere.gitlab.io/Customizations/discord/xmc/colors/amoled.css");
```

![AMOLED color scheme preview](_previews/schemes/amoled.png)

### [Nord](https://www.nordtheme.com)
```css
@import url("https://cynosphere.gitlab.io/Customizations/discord/xmc/colors/nord.css");
```

![Nord color scheme preview](_previews/schemes/nord.png)

## Stylings
Stylings provide extra styling to certain color schemes. They can be used independently of the style they're made for and combined if desired.

### xmc
xmc's stylings forces titlebars to use Segoe UI (the default Windows font) and adds the gradient styling to buttons, scrollbars, selected channels, toolbar buttons and more.
```css
@import url("https://cynosphere.gitlab.io/Customizations/discord/xmc/styling/xmc.css");
```

![xmc Buttons](_previews/stylings/xmc-1.png)

![xmc Toolbar Buttons](_previews/stylings/xmc-2.png)

### Stock
Stock's styling just restores the unread indicators
```css
@import url("https://cynosphere.gitlab.io/Customizations/discord/xmc/styling/stock.css");
```

![Stock Unread Indicators](_previews/stylings/stock.png)

### Custom Background
```css
@import url("https://cynosphere.gitlab.io/Customizations/discord/xmc/styling/custom_background.css");

:root {
  --custom-background: url("url here"); /* theres full access to background syntax in this variable */
}
```

By default, it will inherit the values of the color scheme. You can change the colors as you like by setting `--primary-rgb`, `--secondary-rgb` and `--tertiary-rgb`.
```css
/* remove any scheme colors entirely */
:root {
  --primary-rgb: 0, 0, 0 !important;
  --secondary-rgb: 0, 0, 0 !important;
  --tertiary-rgb: 0, 0, 0 !important;
}
```

You can also change the alpha values with `--primary-alpha`, `--secondary-alpha` and `--tertiary-alpha`. (0-1 decimal value)

![Custom Background Preview](_previews/stylings/custom_background.png)

## Fonts

### [Unifont](http://unifoundry.com/unifont/index.html)
Uses Terminus as monospaced (codeblock) font.
```css
@import url("https://cynosphere.gitlab.io/Customizations/discord/xmc/font/unifont.css");
/* windows users only */
@import url("https://cynosphere.gitlab.io/Customizations/discord/xmc/font/terminus_windows.css");
```

![Unifont Preview](_previews/fonts/unifont.png)

### [Terminus](http://terminus-font.sourceforge.net)
Affects both regular and monospace font.
```css
@import url("https://cynosphere.gitlab.io/Customizations/discord/xmc/font/terminus.css");
/* windows users only */
@import url("https://cynosphere.gitlab.io/Customizations/discord/xmc/font/terminus_windows.css");
```

![Terminus Preview](_previews/fonts/terminus.png)

### Revert Rebrand
Revert Rebrand just makes `--font-display` (Ginto) equal to `--font-primary` (Whitney). This affects various header fonts more than anything.
```css
@import url("https://cynosphere.gitlab.io/Customizations/discord/xmc/font/revert_rebrand.css");
```

![Revert Rebrand Preview](_previews/fonts/revert_rebrand.png)

### The Funny.
You can probably already guess what it is. I will not spoil this one and you're only spoiling yourself if you look at the CSS file [or this preview](_previews/fonts/the_funny.png).
```css
@import url("https://cynosphere.gitlab.io/Customizations/discord/xmc/font/the_funny.css");
```

Alternate monospace font [(preview)](_previews/fonts/the_funny_alt.png)
```css
@import url("https://cynosphere.gitlab.io/Customizations/discord/xmc/font/the_funny_alt.css");
```

## Misc
Misc additions and addons. Some are original, some are just imports to existing theme addons with additions or configuration applied if needed.

### 9x Bot Tag
```css
@import url("https://cynosphere.gitlab.io/Customizations/discord/xmc/misc/9x_bot_tag.css");
```

![9x Bot Tag Preview](_previews/misc/9x_bot_tag.png)

### Mutant Standard Emoji Button
```css
@import url("https://cynosphere.gitlab.io/Customizations/discord/xmc/misc/mutant_standard_emoji_button.css");
```

![Mutant Standard Emoji Button Preview](_previews/misc/mutant_standard_emoji_button-1.png)

![Mutant Standard Emoji Button Preview](_previews/misc/mutant_standard_emoji_button-2.png)

### Nyantro Progress Bar
"I wonder if someone did nyan cat nitro progress bar yet"

```css
@import url("https://cynosphere.gitlab.io/Customizations/discord/xmc/misc/nyantro_progress_bar.css");
```

![Nyantro Progress Bar Preview](_previews/misc/nyantro.gif)

### Old Titlebar
**Has no drag areas, designed to be used with AltDrag!**

```css
@import url("https://cynosphere.gitlab.io/Customizations/discord/xmc/misc/old_titlebar.css");
```

![Old Titlebar Preview](_previews/misc/old_titlebar.png)

### Telegram Image Stacking
Will most likely break if non-images are in a message. Supports both attachments and image links.

```css
@import url("https://cynosphere.gitlab.io/Customizations/discord/xmc/misc/telegram_image_stacking.css");
```

![Telegram Image Stacking Preview](_previews/misc/telegram_image_stacking.png)

### [Friends-Grid](https://github.com/CorellanStoma/Friends-Grid)
Includes extra rules to fix avatar styling to fit with the theme.

```css
@import url("https://cynosphere.gitlab.io/Customizations/discord/xmc/misc/friends_grid.css");
```

### [Context Menu Icons](https://github.com/CorellanStoma/Context-Icons)
Fixes icons in some places (ex: image preview menu). Adds extra icons that may or may not apply to most people. Adds icons to the User Settings Cog context menu. (based off of Settings Icons choices)

```css
@import url("https://cynosphere.gitlab.io/Customizations/discord/xmc/misc/context_menu_icons.css");
```

### [Settings Icons](https://github.com/snappercord/Settings-Icons)
Fork that makes it work without needing Powercord. Also changes some icons and adds some missing icons.

```css
@import url("https://cynosphere.gitlab.io/Customizations/discord/xmc/misc/settings_icons.css");
```

### [Channel Icons](https://github.com/Zer0EqualsFalse/channel-icons)

```css
@import url("https://cynosphere.gitlab.io/Customizations/discord/xmc/misc/channel_icons.css");
```

# Customization
Feel free to look around at the various CSS files and change variables to your liking.
</details>
