Fixed up version of [Settings-Icons](https://github.com/snappercord/Settings-Icons) to load icons from URLs and not need Powercord specific data values. If you use Powercord just use that one, as they do some weird hacks to sass to load SVGs locally and have data values in place.

File path to source.scss to load. Otherwise use settings_icons.css if you don't have sass support.